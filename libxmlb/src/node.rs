// Take a look at the license at the top of the repository in the LICENSE file.

use std::ptr;

use glib::object::IsA;
use glib::translate::*;

use crate::{Node, Query, QueryContext};

impl Node {
    pub fn query_first_with_context(
        &self,
        query: &impl IsA<Query>,
        mut context: Option<&mut QueryContext>,
    ) -> Result<Node, glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::xb_node_query_first_with_context(
                self.to_glib_none().0,
                query.as_ref().to_glib_none().0,
                context.to_glib_none_mut().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    pub fn query_with_context(
        &self,
        query: &impl IsA<Query>,
        mut context: Option<&mut QueryContext>,
    ) -> Result<Vec<Node>, glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::xb_node_query_with_context(
                self.to_glib_none().0,
                query.as_ref().to_glib_none().0,
                context.to_glib_none_mut().0,
                &mut error,
            );
            if error.is_null() {
                Ok(FromGlibPtrContainer::from_glib_container(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }
}
