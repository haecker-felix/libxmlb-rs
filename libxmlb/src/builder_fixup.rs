// Take a look at the license at the top of the repository in the LICENSE file.

use std::boxed::Box as Box_;

use glib::translate::*;

use crate::{BuilderFixup, BuilderNode};

impl BuilderFixup {
    #[doc(alias = "xb_builder_fixup_new")]
    pub fn new<P: Fn(&BuilderFixup, &BuilderNode, Option<&glib::Error>) -> bool + 'static>(
        id: &str,
        func: P,
    ) -> BuilderFixup {
        skip_assert_initialized!();
        let func_data: Box_<P> = Box_::new(func);
        unsafe extern "C" fn func_func<
            P: Fn(&BuilderFixup, &BuilderNode, Option<&glib::Error>) -> bool + 'static,
        >(
            fixup: *mut ffi::XbBuilderFixup,
            node: *mut ffi::XbBuilderNode,
            data: glib::ffi::gpointer,
            error: *mut *mut glib::ffi::GError,
        ) -> glib::ffi::gboolean {
            let fixup = from_glib_borrow(fixup);
            let node = from_glib_borrow(node);
            let callback: &P = &*(data as *mut _);
            let error = Option::<glib::Error>::from_glib_borrow(*error);
            let res = (*callback)(&fixup, &node, error.as_ref().as_ref());
            res.into_glib()
        }
        let func = Some(func_func::<P> as _);
        unsafe extern "C" fn destroy_func<
            P: Fn(&BuilderFixup, &BuilderNode, Option<&glib::Error>) -> bool + 'static,
        >(
            data: glib::ffi::gpointer,
        ) {
            let _callback: Box_<P> = Box_::from_raw(data as *mut _);
        }
        let destroy_call3 = Some(destroy_func::<P> as _);
        let super_callback0: Box_<P> = func_data;
        unsafe {
            from_glib_full(ffi::xb_builder_fixup_new(
                id.to_glib_none().0,
                func,
                Box_::into_raw(super_callback0) as *mut _,
                destroy_call3,
            ))
        }
    }
}
