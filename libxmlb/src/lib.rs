#![cfg_attr(docsrs, feature(doc_cfg))]
#![allow(clippy::needless_doctest_main)]
//! # Rust Xmlb bindings
//!
//! This library contains safe Rust bindings for [Xmlb](https://github.com/hughsie/libxmlb), a library to help create and query binary XML blobs.
//!
//! See also
//!
//! - [gtk-rs project overview](https://gtk-rs.org/)

// Re-export the -sys bindings
pub use {ffi, gio, glib};

/// Asserts that this is the main thread and `gtk::init` has been called.
macro_rules! assert_initialized_main_thread {
    () => {};
}

macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(unused_imports)]
#[allow(clippy::let_and_return)]
#[allow(clippy::type_complexity)]
mod auto;

mod builder_fixup;
mod builder_node;
mod node;
mod silo;
mod stack;

pub use auto::*;

pub mod prelude;
