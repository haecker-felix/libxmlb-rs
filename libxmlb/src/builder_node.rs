// Take a look at the license at the top of the repository in the LICENSE file.

use glib::translate::*;

use crate::BuilderNode;

impl BuilderNode {
    pub fn set_tail(&self, tail: Option<&str>) {
        let tail_len = tail.map(|t| t.len() as isize).unwrap_or(-1);
        unsafe {
            ffi::xb_builder_node_set_tail(self.to_glib_none().0, tail.to_glib_none().0, tail_len);
        }
    }

    pub fn set_text(&self, text: Option<&str>) {
        let text_len = text.map(|t| t.len() as isize).unwrap_or(-1);
        unsafe {
            ffi::xb_builder_node_set_text(self.to_glib_none().0, text.to_glib_none().0, text_len);
        }
    }
}
