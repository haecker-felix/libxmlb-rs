// Take a look at the license at the top of the repository in the LICENSE file.

use std::fmt;

use glib::translate::*;

glib::wrapper! {
    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Stack(Boxed<ffi::XbStack>);

    match fn {
        copy => |ptr| glib::gobject_ffi::g_boxed_copy(ffi::xb_stack_get_type(), ptr as *mut _) as *mut ffi::XbStack,
        free => |ptr| glib::gobject_ffi::g_boxed_free(ffi::xb_stack_get_type(), ptr as *mut _),
        type_ => || ffi::xb_stack_get_type(),
    }
}

impl Stack {
    //#[doc(alias = "xb_stack_pop")]
    // pub fn pop(&mut self, opcode_out: /*Ignored*/Opcode) -> Result<(),
    // glib::Error> {    unsafe { TODO: call ffi:xb_stack_pop() }
    //}

    //#[doc(alias = "xb_stack_push")]
    // pub fn push(&mut self, opcode_out: /*Ignored*/Option<Opcode>) -> Result<(),
    // glib::Error> {    unsafe { TODO: call ffi:xb_stack_push() }
    //}

    #[doc(alias = "xb_stack_to_string")]
    #[doc(alias = "to_string")]
    pub fn to_str(&mut self) -> glib::GString {
        unsafe { from_glib_full(ffi::xb_stack_to_string(self.to_glib_none_mut().0)) }
    }
}

impl fmt::Display for Stack {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Stack")
    }
}
