# libxmlb-rs

The Rust bindings of [libxmlb](https://github.com/hughsie/libxmlb)

Website: <https://world.pages.gitlab.gnome.org/Rust/libxmlb-rs>

## Documentation

- libxmlb: <https://world.pages.gitlab.gnome.org/Rust/libxmlb-rs/stable/latest/docs/libxmlb>
- libxmlb-sys: <https://world.pages.gitlab.gnome.org/Rust/libxmlb-rs/stable/latest/docs/libxmlb_sys>
